# The extension of selenium IDE downlaoded installer package is XPI

# The character used in slants with partial matching of an attribute by xpath us -

# Selenium basic Question

# How is network data capturing done in selenium
By using API in default selenium class

# Which of the following is selenium compatible with
CSS1.0, CSS2.0 and CSS3.0 selectors

# What does the // tell the query?
It needs to stop at the first element that it finds

# DOM refers to Document Object Model

# Table driven framework - Data Driver Framework

# The Command that is beign used to extend the time limit of waitFor command is setTimeout

# What does the runscript adds?
The script tag

# Which selenium tool has a complex API with dictionary based approach
Selenium 1

# When did selenium and webdriver get merged
Year 2008

# What is the diff. between a command and the same command with "AndWait" 
It waits for the result of the "AndWait" command but only upto a maximum of 30s

# Which of the following command is used to compare the actual page title with an expected value?
verifyTitle

# which of the following commands is used to pause execution till the time specified element appear?
waitForElementIfPresent

# which version of opera chromium web browser is not supported by selenium 3?
None of these


# Same origin policy
Selenium RC